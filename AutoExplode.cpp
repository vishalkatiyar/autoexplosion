
#include<stdio.h>
#include <iostream>
#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osgWidget/Util>
#include<osg/Geode>
#include<osg/Geometry>
#include<osg/Group>
#include <osg/NodeVisitor>
#include<osg/Node>
#include <osg/BoundingBox>
#include <List>

#include <osg/BoundingSphere>

#include <osg/MatrixTransform>

#include <osg/Billboard>
#include <algorithm>
#include <osgGA/GUIEventHandler>
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>
#include <osgGA/TrackBallManipulator>
#include <iterator>
#include "../../../include/osgViewer/Viewer"

using namespace std;

//getList
class TraverseNode : public osg::NodeVisitor {
public:
    TraverseNode() :osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN) {
    }
    virtual void apply(osg::Node& node) {
        nod.push_back(&node);
        traverse(node);
    }
    virtual void apply(osg::Geode& geode) {
        lis.push_back(&geode);
        traverse(geode);
    }
    virtual void apply(osg::MatrixTransform& mtran) {
        mt.push_back(&mtran);
        traverse(mtran);
    }

    virtual std::list<osg::Node*> getNodeList() {
        return nod;
    }
    virtual std::list<osg::Geode*> getList() {
        return lis;
    }
    virtual std::list<osg::MatrixTransform*> getList1() {
        return mt;
    }

protected:
    std::list<osg::Node*> nod;
    std::list<osg::Geode*> lis;
    std::list<osg::MatrixTransform*> mt;
};



std::list<osg::MatrixTransform*> getMatrixTransformList(osg::Group* root) {
    TraverseNode tra;
    root->accept(tra);
    return tra.getList1();
}

std::list<osg::Geode*> getGeodeList(osg::Group* root) {
    TraverseNode tra;
    root->accept(tra);
    return tra.getList();
}


void Explode(osg::Group* root, char formate, float dis, double wCenterX, double wCenterY, double wCenterZ) {
    static int upPress = 0;
    //cout << upPress << endl;
    if (upPress > 0) {
        std::list<osg::MatrixTransform*> li1 = getMatrixTransformList(root);
        list <osg::MatrixTransform*> ::iterator mit;
        for (mit = li1.begin(); mit != li1.end(); ++mit) {

            osg::BoundingSphere bs1;
            bs1 = (*mit)->getBound();
            osg::Matrix M;
            {
                if (formate == 'l' || formate == 'L') {
                    M.makeTranslate((bs1.center().x() - wCenterX) + dis, (bs1.center().y() - wCenterY) + dis, (bs1.center().z() - wCenterX) + dis);

                }
                else if (formate == 'm' || formate == 'M') {
                    M.makeTranslate((bs1.center().x() - wCenterX) * dis, (bs1.center().y() - wCenterY) * dis, (bs1.center().z() - wCenterX) * dis);
                }
                (*mit)->setMatrix(M);
            }
        }

    }
    else {
        upPress++;
        std::list<osg::Geode*> li = getGeodeList(root);
        //std::cout << "size of list" << li.size() << endl;
        list <osg::Geode*> ::iterator it;
        for (it = li.begin(); it != li.end(); ++it) {
            //  std::cout <<"getAxis"<< (*it)->getName() << endl;

            osg::Matrix M;
            {

                osg::ref_ptr<osg::MatrixTransform> mt = new osg::MatrixTransform;
               
                osg::BoundingSphere bs, bs1;
                bs = (*it)->getBound();

                if (formate == 'l' || formate == 'L') {
                    M.makeTranslate(
                        (bs.center().x() - wCenterX) + dis, (bs.center().y() - wCenterY) + dis, (bs.center().z() - wCenterX) + dis);
                }
                else if (formate == 'm' || formate == 'M') {
                    M.makeTranslate((bs.center().x() - wCenterX) * dis, (bs.center().y() - wCenterY) * dis, (bs.center().z() - wCenterX) * dis);
                }
                mt->setMatrix(M);
                root->addChild(mt);
                mt->addChild(*it);
                bs1 = mt->getBound();
                for (int itr = 0; itr < (*it)->getNumParents() - 1; itr++)
                {
                    (*it)->getParent(0)->removeChild(*it);
                }
            }
        }
    }
}

void Collapse(osg::Group* root) {

    std::list<osg::MatrixTransform*> li = getMatrixTransformList(root);
    //cout << "Size of list" << li.size() << endl;
    list <osg::MatrixTransform*> ::iterator it;
    for (it = li.begin(); it != li.end(); ++it) {
        osg::BoundingSphere bs1;
        bs1 = (*it)->getBound();
        //cout << bs1.center().x() << " " << bs1.center().y() << " " << bs1.center().z() << endl;
        osg::Matrix M;
        {
            M.makeTranslate(0.0, 0.0, 0.0);
            (*it)->setMatrix(M);
            osg::BoundingSphere bs1;
            bs1 = (*it)->getBound();
            //cout << bs1.center().x() << " " << bs1.center().y() << " " << bs1.center().z() << endl;

        }

    }
}







//Keyboard key handler
class ExpandHandler : public osgGA::GUIEventHandler
{
public:
    ExpandHandler(osg::Group* root, double wCenterX, double wCenterY, double wCenterZ) :
        root(root), wCenterX(wCenterX), wCenterY(wCenterY), wCenterZ(wCenterZ) {}
    virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa) {


        switch (ea.getEventType()) {
        case(osgGA::GUIEventAdapter::KEYDOWN):
        {
            if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Up) {
                if (_expandValue >= 2.0) {
                    _expandValue = 2.0;
                }
                _expandValue += 0.005;
                flag = !flag;
            }
            if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Down) {

                if (_expandValue <= 0) {
                    _expandValue = 0;
                }
                _expandValue -= 0.005;
                flag = !flag;

            }
            if (ea.getKey() == osgGA::GUIEventAdapter::KEY_Left) {
                Collapse(root);
                flag = !flag;
            }


        }
        case(osgGA::GUIEventAdapter::FRAME):
        {
            if (flag) {
                Explode(root, 'm', _expandValue, wCenterX, wCenterY, wCenterZ);
                flag = !flag;
            }


            return true;

        }

        default:
            return false;
        }

    }
private:
    osg::Group* root;
    double wCenterX;
    double wCenterY;
    double wCenterZ;
    double _expandValue = 0.0;
    bool flag = false;

};



int main(int argc, char *argv[])
{
    osg::ref_ptr<osg::Group> root = new osg::Group;
    osg::ref_ptr<osg::Node> lodedModel = osgDB::readNodeFile(argv[1]);
    if (!lodedModel) {
        return 1;
    }
    osgViewer::Viewer viewer;
    root->addChild(lodedModel);
    viewer.setSceneData(root);
    osg::BoundingSphere wbs;
    wbs = root->getBound();
    double wCenterX = wbs.center().x();
    double wCenterY = wbs.center().y();
    double wCenterZ = wbs.center().z();
    viewer.addEventHandler(new ExpandHandler(root, wCenterX, wCenterY, wCenterZ));
    viewer.addEventHandler(new osgGA::StateSetManipulator);
    viewer.addEventHandler(new osgViewer::StatsHandler);
    viewer.setUpViewOnSingleScreen(0);

    return viewer.run();
}

//turbojet engine.ive